# Interactiv4 Template Vars Contracts

Description
-----------
This package contains common interfaces to implement template vars classes.


Versioning
----------
This package follows semver versioning.


Compatibility
-------------
- PHP ^7.1


Installation Instructions
-------------------------
It can be installed manually using composer, by typing the following command:

`composer require interactiv4/template-vars-contracts --update-with-all-dependencies`


Support
-------
Refer to [issue tracker](https://bitbucket.org/interactiv4/template-vars-contracts/issues) to open an issue if needed.


Credits
---------
Supported and maintained by Interactiv4 Team.


Contribution
------------
Any contribution is highly appreciated.
The best way to contribute code is to open a [pull request on Bitbucket](https://bitbucket.org/interactiv4/template-vars-contracts/pull-requests/new).


License
-------
[MIT](https://es.wikipedia.org/wiki/Licencia_MIT)


Copyright
---------
Copyright (c) 2019 Interactiv4 S.L.