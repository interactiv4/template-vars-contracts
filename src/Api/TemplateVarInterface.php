<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\TemplateVars\Api;

/**
 * Interface TemplateVarInterface
 * @api
 */
interface TemplateVarInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string|null $value
     * @return void
     */
    public function setValue(?string $value): void;

    /**
     * @return string|null
     */
    public function getValue(): ?string;

    /**
     * It may require another already set TemplateVar value to retrieve its default value
     *
     * @param TemplateVarsProviderInterface|null $templateVarsProvider
     * @return string|null
     */
    public function getDefaultValue(?TemplateVarsProviderInterface $templateVarsProvider = null): ?string;

    /**
     * @return bool
     */
    public function isCustomizable(): bool;

    /**
     * @return string
     */
    public function __toString(): string;
}
