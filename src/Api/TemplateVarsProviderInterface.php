<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\TemplateVars\Api;

/**
 * Interface TemplateVarsProviderInterface
 * @api
 */
interface TemplateVarsProviderInterface
{
    /**
     * @return TemplateVarInterface[]
     */
    public function getTemplateVars(): array;

    /**
     * @return TemplateVarInterface[]
     */
    public function getCustomizableTemplateVars(): array;

    /**
     * @return TemplateVarInterface[]
     */
    public function getNonCustomizableTemplateVars(): array;

    /**
     * @return string[]
     */
    public function getTemplateVarNames(): array;

    /**
     * @return string[]
     */
    public function getCustomizableTemplateVarNames(): array;

    /**
     * @return string[]
     */
    public function getNonCustomizableTemplateVarNames(): array;

    /**
     * @param string
     * @return TemplateVarInterface|null
     */
    public function getTemplateVarByName(string $templateVarName): ?TemplateVarInterface;

    /**
     * Return names and values in key => value format
     *
     * @return array
     */
    public function toArray(): array;
}
