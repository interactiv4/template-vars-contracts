<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\TemplateVars\Api;

/**
 * Interface TemplateVarsResolverInterface
 * @api
 */
interface TemplateVarsResolverInterface
{
    /**
     * @param TemplateVarsProviderInterface $templateVarsProvider
     * @param string $targetString
     * @return string
     */
    public function resolve(
        TemplateVarsProviderInterface $templateVarsProvider,
        string $targetString
    ): string;
}
