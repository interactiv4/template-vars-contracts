<?php

$rootDir    = __DIR__ . '/..';
$vendorDir  = $rootDir . '/vendor';
$buildDir   = $rootDir . '/build';

$file = $vendorDir . '/autoload.php';
if (!file_exists($file)) {
    throw new RuntimeException('Install dependencies to run test suite.');
}
$autoload = require $file;

if (\is_dir($buildDir)) {
    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($buildDir, RecursiveDirectoryIterator::SKIP_DOTS),
        RecursiveIteratorIterator::CHILD_FIRST
    );

    foreach ($files as $fileinfo) {
        $todo = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
        $todo($fileinfo->getRealPath());
    }
} else {
    // Creating the build dir, to output some potential data, and the code coverage if wanted
    \mkdir($buildDir, 0777);
}